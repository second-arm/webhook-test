package main

import (
	//"crypto/tls"
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"

	"k8s.io/api/admission/v1"

	//appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type patchOperation struct {
	Op    string      `json:"op"`
	Path  string      `json:"path"`
	Value interface{} `json:"value,omitempty"`
}

var patchType *v1.PatchType

func init() {
	p := v1.PatchTypeJSONPatch
	patchType = &p
}

func mutate(req *v1.AdmissionRequest) *v1.AdmissionResponse {
	resp := &v1.AdmissionResponse{
		UID:     req.UID,
		Allowed: true,
	}

	log.Printf("mutate: %v", req.Kind)

	if req.Kind.Kind != "Pod" {
		return resp
	}

	var pod corev1.Pod
	if err := json.Unmarshal(req.Object.Raw, &pod); err != nil {
		resp.Allowed = false
		resp.Result = &metav1.Status{
			Message: err.Error(),
		}

		return resp
	}

	values := map[string]string{}
	for l, v := range pod.Labels {
		values[l] = v
	}
	values["webhook-test"] = "some-value"

	patchOps := []patchOperation{
		patchOperation{
			Op:    "add",
			Path:  "/metadata/labels",
			Value: values,
		},
	}

	patch, err := json.Marshal(patchOps)
	if err != nil {
		resp.Allowed = false
		resp.Result = &metav1.Status{
			Message: err.Error(),
		}

		return resp
	}

	resp.Patch = patch
	resp.PatchType = patchType

	return resp
}

func handler(w http.ResponseWriter, r *http.Request) {
	var body []byte

	log.Printf("input %q", r.URL.Path)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "empty body", http.StatusBadRequest)
		return
	}

	if ctype := r.Header.Get("Content-Type"); ctype != "application/json" {
		log.Printf("unexpected content-type %q", ctype)
		http.Error(w, "invalid content-type", http.StatusUnsupportedMediaType)
		return
	}

	input := v1.AdmissionReview{}
	if err := json.Unmarshal(body, &input); err != nil {
		log.Printf("unmarshal body error %v", err)
		http.Error(w, "bad body", http.StatusBadRequest)
		return
	}

	output := input
	output.Request = nil
	output.Response = mutate(input.Request)

	body, err = json.Marshal(output)
	if err != nil {
		log.Printf("response marshal error %v", err)
		http.Error(w, "marshal error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)
}

func main() {
	var (
		certFile string
		keyFile  string
		addr     string
	)

	flag.StringVar(&addr, "addr", ":443", "listen")
	flag.StringVar(&certFile, "cert", "", "certificate file")
	flag.StringVar(&keyFile, "key", "", "key file")

	flag.Parse()

	http.ListenAndServeTLS(addr, certFile, keyFile, http.HandlerFunc(handler))
}
